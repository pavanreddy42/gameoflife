#!/usr/bin/env python
import json
import requests
import os


api_url = ('https://api.bitbucket.org/2.0/repositories/''pavanreddy42/gameoflife/commit/%(revision)s/statuses/'
				 % {'revision': os.environ['GIT_COMMIT']})
	  
response=requests.get(api_url,auth=('pavanreddy42','pavan123'), headers = {'content-type': 'application/json'})
data = (response.json())
with open('data.json', 'w+') as f:
   json.dump(data, f)
	  
data = json.load(open('data.json'))
n = data["size"]
print(n)
for x in range (0,n):
	state = data["values"][x]["state"]
	if state != "SUCCESSFUL":
			key = data["values"][x]["key"]
			print(key)
			file = {
					'key':key,
				    'state': 'SUCCESSFUL',
					'url':'http://ec2-54-163-127-188.compute-1.amazonaws.com:8080/',
					'description': 'The build passed.'
						   }
			api_url = ('https://api.bitbucket.org/2.0/repositories/''pavanreddy42/gameoflife/commit/%(revision)s/statuses/build'
							  %  {'revision': os.environ['GIT_COMMIT']})  
			response1=requests.post(api_url,auth=('pavanreddy42','pavan123'), headers = {'content-type': 'application/json'}, json=file)
			print(response1.content)
			  
	print("build is SUCCESSFUL")
		   
		   
a = os.environ['pullRequestId']
print(a)
if a:
	api_url2 = ('https://api.bitbucket.org/2.0/repositories/''pavanreddy42/gameoflife/pullrequests/%(pullrequestid)s/approve/'
					 %{ 'pullrequestid': os.environ['pullRequestId']})
	response2=requests.post(api_url2, auth=('pavanreddy42','pavan123'))
	data2 = (response2.content)
	print(data2)
else :
	print("NO pullrequest number specified to approve")
